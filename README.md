# KOIN SAMPLE #
---

Basic tutorial to insert koin in a project.

Following Ray Wenderlich tutorial 

https://www.raywenderlich.com/9457-dependency-injection-with-koin


### DEPENDENCY INJECTION ###

**DI** concept becomes with the idea of dependency. When a **class A** need to use a reference of **class B**, this means that **class A** has a dependency of **class B**.
An example more illustrated, imagine we have a class **Car** that might need a reference of class **Engine** to works. These required classes are called *dependencies* so class **Car** is dependant to have an instance of class **Engine** to run.

One way to get this dependency is with *dependency injection*, that means that **Class Car** has an instance of **class Engine** and doesn't know where it comes from.

![Dependency](https://miro.medium.com/max/800/1*R-CmfcmbvfDK8eAkhK0gXg.jpeg)


### PROS AND CONS ###

Using dependency injection helps our projects to follow the **SOLID** principles. In this case in particular, we take advantge of the *Single Responsibility principle* and *Dependency inversion*.

 **Single Responsibility** -> Every class or module is responsible for just a singles piece of program's function
 **Dependency Inversion** -> High level (*View*) should not know on low level(*business logic*) modules. Both depends on abstractions. 

 For more information about [**SOLID**](https://itnext.io/solid-principles-explanation-and-examples-715b975dcad4) principles
 
### Steps to use KOIN ###

**1. Declare a module** (*Define those entities which will be injected in some point of the app*)

```kotlin
val applicationModule = module {
	single { AppRepository }
}
```

**2. Start koin** (*Allows you to launch the DI process and indicate which modules will be available when needed, in this case only applicationModule*)

```kotlin
class BaseApplication : Application() {
  override fun onCreate() {
    super.onCreate()
    startKoin(this, listOf(applicationModule))
  }
}
```

**3. Perform an injection** (*Allows to perform a lazy injection*)

```kotlin 
class FeatureActivity : AppCompatActivity() {
  private val appRepository: AppRepository by inject()
  ...
}
```

Koin allows to classes to conform the *koinComponent* interface so the injections are possible in non-Activity classes.

The dependencies in this project are between **Views** and **Presenters** and **Presenters** and **Repository** (also Shared Preferences and Database). That is because of the **MVP** architecture.

![MVP architecture](https://felarmir.com/img/posts/mvp_md.png)

### MODULE ###

```kotlin
val applicationModule = module(override = true) {

    factory<SplashContract.Presenter> { (view: SplashContract.View) -> SplashPresenter(view) }

    factory<MainContract.Presenter> { (view: MainContract.View) -> MainPresenter(view) }

    factory<FeatureContract.Presenter<Student>> { (view: FeatureContract.View<Student>) -> FeaturePresenter(view) }

    single<FeatureContract.Model<Student>> { AppRepository }

    single<SharedPreferences> { androidContext().getSharedPreferences("SharedPreferences", Context.MODE_PRIVATE) }

    single { Room.databaseBuilder(androidContext(), AppDatabase::class.java, "app-database").build() }
    
}
```

* **factory** -> Is a definition that will give you a new instance each time you ask for this object type.
* **single** ->  A singleton component such as an instance that is unique across the application. This is typically intended for repositories, databases, etc.

> **NOTE** Koin single and factory object declarations allow you to include a type in angle brackets and a lambda expression, which defines the way the object will be constructed. Due to SOLID principles, the indicated type is usually an interface that the object to inject has to implement. This makes this object easily exchangeable in the future. For example, in the first case the SplashPresenter needs to implement the SplashContract.Presenter and will use a SplashContract.View object as an argument constructor.

In other words, I will create an instance of the interface (implemented in the Presenter), each time I need to use from The Activity/Fragment.

For example in SplashScreen case:

* In my **module** ->  ```factory<SplashContract.Presenter> { (view: SplashContract.View) -> SplashPresenter(view) }```

* In my **presenter** -> ```class SplashPresenter(private var view: SplashContract.View?)``` (The constructor I need to know to set the view in module)

* In my **activity** -> ```private val splashPresenter : SplashContract.Presenter by inject { parametersOf(this) }```

### CONCLUSION ###

In my Activity I have an instance of the Presenter interface, implemented by the Presenter and injected by the module. 

So instead of having a constructor of my Presenter I have an instance of the Presenter interface that is constructed in thge module and implemtned in the Presenter.
