package com.raywenderlich.markme.di

import android.arch.persistence.room.Room
import android.content.Context
import android.content.SharedPreferences
import com.raywenderlich.markme.feature.FeatureContract
import com.raywenderlich.markme.feature.presenter.FeaturePresenter
import com.raywenderlich.markme.main.MainContract
import com.raywenderlich.markme.main.presenter.MainPresenter
import com.raywenderlich.markme.model.Student
import com.raywenderlich.markme.model.database.AppDatabase
import com.raywenderlich.markme.repository.AppRepository
import com.raywenderlich.markme.splash.SplashContract
import com.raywenderlich.markme.splash.presenter.SplashPresenter
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

/**
 * With override = true this makes to override any other definition of this module
 */
val applicationModule = module (override = true) {

    /**
     * factory -> Definition that will give you a new instance each time you ask for this object type
     * single -> Create a singleton object that is unique for all the application
     */

    /**
     *  Object declarations allow you to include a type in angle brackets and a lambda expression,
     *  which defines the way the object will be constructed
     *
     *  The type indicated usually is an interface and the object that has to implement
     *
     *  SplashPresenter needs to implement the SplashContract.Presenter and will use SplashContract.View
     *  object as an argument constructor
     *
     *  Each time I instantiate a SplashPresenter.Presenter I make trough the SplashPresenter constructor
     */

    factory<SplashContract.Presenter> { (view: SplashContract.View) -> SplashPresenter(view) }

    factory<MainContract.Presenter> { (view: MainContract.View) -> MainPresenter(view) }

    factory<FeatureContract.Presenter<Student>> { (view: FeatureContract.View<Student>) -> FeaturePresenter(view) }

    single<FeatureContract.Model<Student>> { AppRepository }

    single<SharedPreferences> { androidContext().getSharedPreferences("SharedPreferences", Context.MODE_PRIVATE) }

    single {
        Room.databaseBuilder(androidContext(),
                AppDatabase::class.java, "app-database").build()
    }
}